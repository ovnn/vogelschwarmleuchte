
/*
 Name:		bird01.ino
 Created:	21.07.2019 14:15:07
 Author:	Thk
*/
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WEMOS_Matrix_LED.h>

MLED mled(1); //set intensity=5
struct Eye
{
	int x;
	int y;
};
struct Eye Eyepos;







void drawLinie(int row, int collumStart, int collumEnd, int draw, int delaytime) {
	
	for (int i = collumStart; i <= collumEnd; i++) {
		mled.dot(i,row, draw);
	}
	mled.display();
	delay(delaytime);
}

void drawEye(int x, int y, int draw) {
  for (int x1=x; x1<(x+3);x1++){
    for (int y1=y; y1<(y+3);y1++){
    mled.dot(x1, y1, draw);
  }
  }
	
}

void zwinker_eye(int delaytime) {
	int color = 0;

	drawLinie(0, 0, 7, color, delaytime);
	drawLinie(7, 0, 7, color, delaytime);

	drawLinie(1, 0, 7, color, delaytime);
	drawLinie(6, 0, 7, color, delaytime);

	drawLinie(2, 0, 7, color, delaytime);
	drawLinie(5, 0, 7, color, delaytime);

	drawLinie(3, 0, 7, color, delaytime);
	drawLinie(4, 0, 7, color, delaytime);

	color = 1;

	drawLinie(3, 0, 7, color, delaytime);
	drawLinie(4, 0, 7, color, delaytime);

	drawLinie(2, 0, 7, color, delaytime);
	drawLinie(5, 0, 7, color, delaytime);

	drawLinie(1, 0, 7, color, delaytime);
	drawLinie(6, 0, 7, color, delaytime);

	drawLinie(0, 0, 7, color, delaytime);
	drawLinie(7, 0, 7, color, delaytime);


}



void fill() {
	for (int x = 0; x < 8; x++) {
		for (int y= 0; y < 8; y++) {
			mled.dot(x, y, 1);
		}
	}
}

struct Eye chk_eye_move(int direction, int x, int y) {
	struct Eye eyetemp;
	eyetemp.x = -1;
	eyetemp.y = -1;

	if (direction == 1) {  // Up + Left
		x -= 1;
		y -= 1;
	}
	if (direction == 2) { // Up + Left
		y -= 1;
	}
	if (direction == 3) {
		// Up + Left
		x += 1;
		y -= 1;
	}
	if (direction == 4) { // Up + Left
		x += 1;
	}
	if (direction == 5) { // Up + Left
		x += 1;
		y += 1;
	}
	if (direction == 6) { // Up + Left
		y += 1;
	}
	if (direction == 7) { // Up + Left
		x -= 1;
		y += 1;
	}
	if (direction == 8) { // Up + Left
		x -= 1;
	}
	if ((x < 1) || x > 4 || y < 1 || y > 4) {

		return eyetemp;
	}


	eyetemp.x = x;
	eyetemp.y = y;

	return eyetemp;
}


void setup() {


	// put your setup code here, to run once:
	Eyepos.x = 3;
	Eyepos.y = 3;

	

	Serial.println("Booting");
	WiFi.forceSleepBegin();
}

void loop() {
  fill();



	int richtung = random(1, 100);

	if (richtung < 9) {
		struct Eye EyeposNew = chk_eye_move(richtung, Eyepos.x, Eyepos.y);
		if (EyeposNew.x != -1 && EyeposNew.y != -1) {
			Eyepos.x = EyeposNew.x;
			Eyepos.y = EyeposNew.y;
		}

	}

	drawEye(Eyepos.x, Eyepos.y, 0);
	mled.display();
	

	if (random(0, 30) == 0) {
		zwinker_eye(20);
		drawEye(Eyepos.x, Eyepos.y, 0);
  }
		mled.display();
    	mled.display();
	WiFi.forceSleepBegin();

	delay(100);

}

















